This is a **community** effort. It isn't meant to be the opinions of 1 person. As such, community contributing is integral to having a reasonable blocklist.

## Raison D'être & Goals

This project was created in order to be more cognizant and hesitant about blocking instances. This includes making sweeping generalizations, blocking entire instances due to one or a few users, or attempting to leverage inocuous "evidence".

The long-term goal is to create a better, as-objective-as-possible standard for:

* evaluating/assessing flagged instances
* categorizing/classifying instances
* replacing vague reasoning for blocking with cogent, explicated terminology

---

As Mastodon continues to grow in popularity, it becomes more important to address unnecessary blocking.

As always, feedback and open discussion is strongly encouraged.


### Creating new issues

This repository is mirrored on Github. Please [create issues on Gitlab instead](https://gitlab.com/andrewzah/better-mastodon-blocklist/issues).
